<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields(get_the_ID());

$context['is_preview'] = $is_preview;

$context['related'] = Timber::get_posts( [
    'post_type' => 'post',
    'posts_per_page' => 3,
    'post__not_in' => [ get_the_ID() ],
    'tag__not_in' => [ 13 ]
]);

$context['categories'] =  Timber::get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => true,
));

$context['posts'] =  Timber::get_posts( [
    'post_type' => 'post',
    'posts_per_page' => 9,
    'tag__not_in' => [ 13 ]
] );


Helper::output_this_block_css('blog-archive');

Timber::render( 'template.twig', $context);