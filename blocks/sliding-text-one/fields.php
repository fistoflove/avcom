<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["One", "text"],
        ["Two", "text"],
        ["Three", "text"],
        ["Four", "text"],
        ["Five", "text"],
        ["Link", "link"],
    ]
);