<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Info",
    [
        ["Title", "text"],
        ["Date", "text"],
        ["Location", "text"],
        ["Content", "wysiwyg"]
    ]
);