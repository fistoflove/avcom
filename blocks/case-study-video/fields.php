<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Video",
    [
        ["Video URL", "text"],
        ["Thumbnail", "image"],
    ]
);