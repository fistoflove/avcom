<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Text", "wysiwyg"],
        ["Items", "repeater", [
            ["Title", "text"],
            ["Inner", "wysiwyg"]
        ]],
    ]
);

$fields->register_tab(
    "CTA",
    [
        ["Title", "text"],
        ["Form", "clone", "group_633d4a849c668"],
    ]
);
