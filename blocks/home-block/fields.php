<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Color", "clone", "group_62d9bc6dc3a05"],
        ["Items", "repeater", [
            ["Item", "link"],
            ["Color", "clone", "group_62d9bc6dc3a05"]
        ]],
    ]
);