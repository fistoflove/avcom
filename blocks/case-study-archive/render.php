<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields(get_the_ID());

$context['is_preview'] = $is_preview;

$context['categories'] =  Timber::get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => true,
));

$context['posts'] =  Timber::get_posts( [
    'post_type' => 'case-study',
    'posts_per_page' => 99
] );


Helper::output_this_block_css('case-study-archive');

Timber::render( 'template.twig', $context);