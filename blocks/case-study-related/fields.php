<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Slider",
    [
        ["Slides", "repeater", [
            ["Slide", "image"]
        ]],
    ]
);