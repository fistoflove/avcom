<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;
use IMSWP\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;



$context['block_path']  = "/wp-content/themes/cec/blocks/become-a-host";

$context['jobs'] =  Timber::get_posts( [
    'post_type' => 'job',
    'posts_per_page' => -1
] );

add_filter( 'timber/twig', function( $twig ) {
    $twig->addFunction( new Twig_Function( 'attribute_len', function($text, $min = 156, $max = 500) {
        if(is_string($text)) {
            $len = strlen($text);
            $text_width = $len * 10;
            return $text_width * 1.1;
        }
    } ) );
    return $twig;
} );

Timber::render( 'template.twig', $context);