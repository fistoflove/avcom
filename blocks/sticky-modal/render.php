<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;
Helper::output_this_block_css('sticky-modal');
Helper::output_this_block_css('whitepaper-form-new');

Timber::render( 'template.twig', $context);