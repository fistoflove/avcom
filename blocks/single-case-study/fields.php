<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Slider",
    [
        ["Slides", "repeater", [
            ["Slide", "image"]
        ]],
    ]
);

$fields->register_tab(
    "Video",
    [
        ["Video URL", "text"],
        ["Title", "text"],
        ["Subtitle", "text"]
    ]
);

$fields->register_tab(
    "Info",
    [
        ["Title", "text"],
        ["Date", "text"],
        ["Location", "text"],
        ["Content", "wysiwyg"]
    ]
);

$fields->register_tab(
    "Content One",
    [
        ["Content", "wysiwyg"],
        ["Image", "image"]
    ]
);

$fields->register_tab(
    "Content Two",
    [
        ["Content", "wysiwyg"],
    ]
);
