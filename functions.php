<?php

use Timber\Timber;

// Register custom post types
require_once('register-custom-post-types.php');

function acf_filter_rest_api_preload_paths( $preload_paths ) {
    if ( ! get_the_ID() ) {
      return $preload_paths;
    }
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
    $v1 =  array_filter(
      $preload_paths,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
    return array_filter(
      $v1,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
  }
add_filter( 'block_editor_rest_api_preload_paths', 'acf_filter_rest_api_preload_paths', 10, 1 );


function remove_menus(){
  remove_menu_page( 'edit-comments.php' );          //Comments  
}

if ( current_user_can( 'editor' ) ){
    add_action( 'admin_menu', 'remove_menus' );
}

/**
 * Disable the emoji's
 */
 add_action( 'init', function() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
  add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
 } );

 /**
  * Filter function used to remove the tinymce emoji plugin.
  * 
  * @param array $plugins 
  * @return array Difference betwen the two arrays
  */

 function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
  return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
  return array();
  }
 }
 
 /**
  * Remove emoji CDN hostname from DNS prefetching hints.
  *
  * @param array $urls URLs to print for resource hints.
  * @param string $relation_type The relation type the URLs are printed for.
  * @return array Difference betwen the two arrays.
  */

 function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
  if ( 'dns-prefetch' == $relation_type ) {
  /** This filter is documented in wp-includes/formatting.php */
  $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
 
 $urls = array_diff( $urls, array( $emoji_svg_url ) );
  }
 return $urls;
 }

 $cats = wp_get_post_categories( get_the_ID() );  //post id
 foreach($cats as $c) {
     $ancestors = array_merge( array_reverse( get_ancestors( $c, 'category' ) ), [$c] );
     echo '<ul class="post-categories">';
     foreach($ancestors as $id){
         echo '<li><a href="' . esc_url( get_category_link( $id) ) . '">' . get_cat_name( $id ) . '</a></li>';
     }
     echo '</ul>';
 }



add_filter( 'timber/twig', 'add_to_twig_theme' );

function add_to_twig_theme( $twig ) {
  $twig->addFunction( new Twig_Function( '_ims_get_videos', '_ims_get_videos' ) );
//  $twig->addFunction( new Twig_Function( 'get_post_primary_category', 'get_post_primary_category' ) );
  return $twig;
}

add_action( 'wp_ajax_nopriv_ims_get_videos', 'ims_get_videos' );

add_action( 'wp_ajax_ims_get_videos', 'ims_get_videos' );


function ims_get_videos() {
  $videos = explode(',', $_REQUEST['categories']);
  $context = Timber::get_context();

  $context['videos'] = Timber::get_posts([
      'post_type' => 'video',
      'posts_per_page' => -1,
      'order' => 'ASC',
      'post__in' => $videos
  ]);
  Timber::render( 'service-video-one-ajax.twig', $context );
  die();
}

function get_post_primary_category_extra($post_id, $term='category', $return_all_categories=false){
  $return = array();

  if (class_exists('WPSEO_Primary_Term')){
      // Show Primary category by Yoast if it is enabled & set
      $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
      $primary_term = get_term($wpseo_primary_term->get_primary_term());

      if (!is_wp_error($primary_term)){
          $return['primary_category'] = $primary_term;
      }
  }

  if (empty($return['primary_category']) || $return_all_categories){
      $categories_list = get_the_terms($post_id, $term);

      if (empty($return['primary_category']) && !empty($categories_list)){
          $return['primary_category'] = $categories_list[0];  //get the first category
      }
      if ($return_all_categories){
          $return['all_categories'] = array();

          if (!empty($categories_list)){
              foreach($categories_list as &$category){
                  $return['all_categories'][] = $category->term_id;
              }
          }
      }
  }

  return $return;
}

function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
  $return = array();

  if (class_exists('WPSEO_Primary_Term')){
      // Show Primary category by Yoast if it is enabled & set
      $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
      $primary_term = get_term($wpseo_primary_term->get_primary_term());

      if (!is_wp_error($primary_term)){
          $return['primary_category'] = $primary_term;
      }
  }

  if (empty($return['primary_category']) || $return_all_categories){
      $categories_list = get_the_terms($post_id, $term);

      if (empty($return['primary_category']) && !empty($categories_list)){
          $return['primary_category'] = $categories_list[0];  //get the first category
      }
      if ($return_all_categories){
          $return['all_categories'] = array();

          if (!empty($categories_list)){
              foreach($categories_list as &$category){
                  $return['all_categories'][] = $category->term_id;
              }
          }
      }
  }

  return $return;
}
// WP Admin bar style fix

add_action('wp_head', function() {
  if(is_user_logged_in()) {
    echo "<style>.header { top:32px;}</style>";
  }
});

// Google Recaptcha performance fix

add_action('wp_head', function () {
  $page_id = get_queried_object_id();
  if($page_id != 209) {
    wp_deregister_script('wpcf7-recaptcha');
    wp_deregister_script('google-recaptcha');
    wp_dequeue_script('wpcf7-recaptcha');
    wp_dequeue_script('google-recaptcha');
    wp_dequeue_script('google-invisible-recaptcha' );
  }
});

add_filter( 'timber/twig', function( $twig ) {
  $twig->addFunction( new Twig_Function( 'getPosts', function( $category ) {
      $post = Timber::get_posts( [
          'post_type' => 'post',
          'posts_per_page' => -1,
          'tag__not_in' => [ 13 ],
          'tax_query' => [
              [
                  'taxonomy' => 'category',
                  'field'    => 'term_id',
                  'terms'    => $department->id,
              ]
          ]
      ] );
      return $posts;
  } ) );
  return $twig;
});

add_action( 'wp_ajax_nopriv_get_posts_ajax', 'get_posts_ajax' );

add_action( 'wp_ajax_get_posts_ajax', 'get_posts_ajax' );

function get_posts_ajax() {
  $categories = explode(',', $_REQUEST['categories']);
  $context = Timber::get_context();
  
  $context['ajax_posts'] = [];

  foreach($categories as $cat) {

    $posts = Timber::get_posts([ 'post_type' => 'post', 'tag__not_in' => [ 13 ], 'posts_per_page' => -1, 'category' => $cat ]);

    foreach($posts as $post) {
      array_push($context['ajax_posts'], $post);
    }
  }

  Timber::render( 'posts-ajax.twig', $context );
  die();
}

add_action('wp_head', function() {
?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/avcom/favicon.ico">
<meta name="msapplication-TileColor" content="#1D1D1B">
<meta name="theme-color" content="#1D1D1B">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N75QR75');</script>
<!-- End Google Tag Manager -->
<?php
});

add_action('wp_footer', function() {
  ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.11.2/gsap.min.js" defer></script>
  <?php
});

function parse_inner_blocks(&$parsed_block)
{
    if (isset($parsed_block['innerBlocks'])) {
        foreach ($parsed_block['innerBlocks'] as $key => &$inner_block) {
            if (!empty($inner_block['innerContent'])) {
                foreach ($inner_block['innerContent'] as &$inner_content) {
                    if (empty($inner_content)) {
                        continue;
                    }

                    $inner_content = do_shortcode($inner_content);
                }
            }
            if (isset($inner_block['innerBlocks'])) {
                $inner_block = parse_inner_blocks($inner_block);
            }
        }
    }

    return $parsed_block;
}

add_filter('render_block_data', function ($parsed_block) {

    if (isset($parsed_block['innerContent'])) {
        foreach ($parsed_block['innerContent'] as &$inner_content) {
            if (empty($inner_content)) {
                continue;
            }

            $inner_content = do_shortcode($inner_content);
        }
    }

    $parsed_block = parse_inner_blocks($parsed_block);

    return $parsed_block;
}, 10, 1);