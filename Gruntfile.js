module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      dev: {
        files: {
          // "static/hubspot-header.css": "dev/scss/header-hubspot.scss",
          // "static/hubspot-footer.css": "dev/scss/footer-hubspot.scss",
          "static/main.css": "dev/scss/source.scss",
          "static/orphan.css": "dev/scss/orphan-pages.scss",
          "static/mold.css": "../mold/dev/scss/source.scss",
          "static/blog.css": "dev/scss/blog.scss",
          "case-study/blog.css": "dev/scss/case-study.scss",
          "static/typography.css": "dev/scss/typography.scss",
        },
      },
    },
    csso: {
      compress: {
        options: {
          report: "gzip",
        },
        files: {
          // "static/header-hubspot.min.css": [
          //   "static/hubspot-header.css",
          //   "static/mold.css",
          //   "static/typography.css"
          // ],
          // "static/footer-hubspot.min.css": [
          //   "static/hubspot-footer.css",
          //   "static/mold.css",
          //   "static/typography.css"
          // ],
          "static/main.min.css": ["static/main.css"],
          "static/typography.min.css": ["static/typography.css"],
          "static/orphan.min.css": ["static/orphan.css"],
          "static/base.min.css": [
            "static/main.css",
            "static/typography.css"
          ]
        },
      },
    },
    watch: {
      files: ["dev/scss/*", "dev/scss/theme/*"],
      tasks: ["sass", "csso"],
    },
  });
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-csso");
  grunt.loadNpmTasks("grunt-contrib-watch");
};

